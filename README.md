# Testudo
```
How to Install, Build, and Run: 
Git clone the repository: git clone https://bluemelodia@bitbucket.org/bluemelodia/shelldivision.git

iOS version is in the ShellDivision folder. 
1) To run the iOS version, you need a Mac. I used MacBook Pro (Retina, 13-inch), running on 2.6 GHz Intel Core i5, with operating system OS X El Capitan, Version 10.11.3. 
2) Go to the App Store and download latest version of XCode. I used Version 7.3. 
3) In Xcode, go to File -> Open and select the ShellDivision folder. 
4) In the top bar (see image), select iPhone 6s from the list of simulators. 
![Screen Shot 2016-05-09 at 5.01.13 PM.png](https://bitbucket.org/repo/qkyXn6/images/806235700-Screen%20Shot%202016-05-09%20at%205.01.13%20PM.png)
5) Press the play button in the top bar to run the app. The simulator will launch and it will load the app. 
![Screen Shot 2016-05-09 at 5.03.17 PM.png](https://bitbucket.org/repo/qkyXn6/images/3259111935-Screen%20Shot%202016-05-09%20at%205.03.17%20PM.png) 

Android version is in the ShellDivAndroid folder
1) To run the Android version, you need an Android device (this app will not run on the emulator), and you need to enable developer options on your device (there are online tutorials on how to do so for your specific device). I used a Xiaomi HM Note 1 LTE. 
2) Go to http://developer.android.com/sdk/index.html and download Android Studio. 
3) In Android Studio, go to File -> Open and select ShellDivAndroid folder. If you receive message "Unregistered VCS root detected: The directory <name of directory> is under Git, 
but is not registered in the Settings," then click Configure. In the menu you will see a list of un-registered roots, click on the un-registered roots and press the "+" button to 
register them. Press ok. Check David Argyle Thacker's post on Stack Overflow (http://stackoverflow.com/questions/27690431/configure-intellij-idea-such-that-source-detected-in-git-but-unregistered-vcs-ro)
for the screenshots. Press "Project" in the left sidebar to view the directory structure. 
4) Connect the Android phone to the computer, and press the play button on the top bar to run the app. 
![Screen Shot 2016-05-09 at 5.15.46 PM.png](https://bitbucket.org/repo/qkyXn6/images/2794499907-Screen%20Shot%202016-05-09%20at%205.15.46%20PM.png)
5) In the Device Chooser popup, select your device and press the "OK" button. 
![Screen Shot 2016-05-09 at 6.16.00 PM.png](https://bitbucket.org/repo/qkyXn6/images/3644506903-Screen%20Shot%202016-05-09%20at%206.16.00%20PM.png)
6) Wait for the "Install blocked" countdown to time out (takes 10 seconds), press "Allow once" to run the app.
![S__7217166.jpg](https://bitbucket.org/repo/qkyXn6/images/948865565-S__7217166.jpg) 
7) Press "Install" when you see the "Permission request from Unidentified PC tools" screen. The app will appear after a few seconds delay.
![S__7217165.jpg](https://bitbucket.org/repo/qkyXn6/images/2829428728-S__7217165.jpg)

Functionality: I built a two-player strategy game, Shell Division, on both iOS (using XCode) and Android (using Android Studio). The player takes control of either the Snapping Turtle or Sea Turtle species, and their goal is to make their species into the dominant species. The hardware used was iOS6 device and the corresponding simulator for iOS6, and an Xiaomi HM Note 1 LTE. Because the Android emulator was unable to support multiple images without incurring an out-of-memory error, I decided to run the final product on the actual mobile devices only. 

For each version of the game, the following functionality was implemented:
•	Basic game mechanics: the goal of the game is to become the dominant species (you have more organisms on the board than the other player does) in the habitat (8x8 grid).
•	The game ends when the time period reaches the modern era (it starts at 300mya and decrements by 1 million years per turn), or when the grid is completely full. 
•	When a member of your species is surrounded by 5 or more members of the other player’s species, your piece “defects” to the other player and has its species changed accordingly. This can lead to cascades of “defection” events, where large numbers of a player’s pieces are lost to the other player, and forces players to be strategic about how they arrange their organisms on the grid. 
•	The game’s state will be saved (persistent game state storage). 
•	There is an option to restart the game. 
•	The user interface is complete.
```